# 纯仓颉实现的XXHash（64 bits version）

# 用法
```cangjie
let s = "example data"
let bytes = s.toArray()
let xxhash = XXHash64(0)
let res = xxhash.sum(bytes)
assertEqual("data", "res", res, 9313625240855019448)
```